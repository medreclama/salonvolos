import GLightbox from 'glightbox';

const lb = () => {
  const lightbox = GLightbox({
    selector: '[data-glightbox]',
    moreLength: 0,
  });

  const lbItems = Array.from(document.querySelectorAll('[data-glightbox]')).filter((item) => !item.dataset.gallery);

  if (lbItems.length) {
    lbItems.forEach((item) => {
      item.addEventListener('click', (e) => {
        e.preventDefault();
        const targetHref = e.currentTarget.getAttribute('href');
        const description = e.currentTarget.getAttribute('data-description') || ''; // Извлечение описания
        lightbox.setElements([{
          href: targetHref,
          description,
        }]);
        lightbox.open();
      });
    });
  }
};

export default lb;
