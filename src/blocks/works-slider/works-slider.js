import Slider from '../slider/slider';

const worksSlider = document.querySelector('.works-slider');

const worksSliderInit = () => {
  if (!worksSlider) return;
  const worksSliderSlides = worksSlider.querySelector('.works-slider .slider__slides');
  const sliderImage = worksSlider.querySelector('.slider__slides img');
  const sliderVideo = worksSlider.querySelector('.slider__slides video');
  const sliderDescription = worksSlider.querySelector('.works-slider__description');
  const worksSliderInstance = new Slider(worksSliderSlides, {
    slidesPerView: 1,
    on: {
      slideChange: () => {
        if (!sliderVideo.paused) sliderVideo.pause();
      },
    },
  });
  worksSliderInstance.init();
  const closeButton = document.createElement('button');
  closeButton.classList.add('works-slider-button');
  worksSlider.appendChild(closeButton);

  const onWorkHandle = (evt) => {
    const work = evt.target.closest('.works-item');
    if (work) {
      worksSlider.classList.add('works-slider--active');
      sliderImage.src = work.querySelector('source[type="image/webp"]').srcset;
      sliderVideo.src = work.dataset.video;
      sliderVideo.load();
      if (work.dataset.description !== '') {
        sliderDescription.classList.add('works-slider__description--active');
        sliderDescription.innerHTML = work.dataset.description;
      } else sliderDescription.classList.remove('works-slider__description--active');
      document.body.style.overflow = 'hidden';
    }
  };

  document.addEventListener('click', onWorkHandle);

  closeButton.addEventListener('click', () => {
    worksSlider.classList.remove('works-slider--active');
    sliderImage.src = '';
    sliderVideo.src = '';
    document.body.style.overflow = 'auto';
    setTimeout(() => {
      worksSliderInstance.slider.slideTo(0);
    }, 500);
  });
};

export default worksSliderInit;
