/* eslint-disable no-new */
import header from '../blocks/header/header';
import headerNavigation from '../blocks/header-navigation/header-navigation';
import headerSearch from '../blocks/header-search/header-search';
import Slider from '../blocks/slider/slider';
import parallax from './modules/parallax';
import appear from '../blocks/appear/appear';
import saturate from '../blocks/saturate/saturate';
import lb from '../blocks/lightbox/lightbox';
import countdown from '../blocks/countdown/countdown';
import hairExtentionSign from '../blocks/hair-extention-sign/hair-extention-sign';
import modal from '../blocks/modal/modal';
import { accordionItem, accordionMenu } from '../blocks/accordion-item/accordion-item';
import footerDate from '../blocks/footer/footer';
import Select from '../blocks/form-input/form-input';
import LoadingMoreItems from './modules/LoadingMoreItems';
import serviceNavigationInit from '../blocks/service-navigation/service-navigation';
import ratingInit from '../blocks/rating/rating';
import MultiStepCalculator from '../blocks/multi-step-calculator/multi-step-calculator';
import MultiStepCalculatorPrices from '../blocks/multi-step-calculator/multi-step-calculator-prices';
import mrcSH from '../blocks/show-hide/show-hide';
import replaceRubleSymbols from './modules/helpers';
import MultiStepCalculatorForm from '../blocks/multi-step-calculator-form/multi-step-calculator-form';
import video360Init from '../blocks/video-js/video-js';
import worksSliderInit from '../blocks/works-slider/works-slider';

const selects = document.querySelectorAll('select.form-input__field');
selects.forEach((select) => {
  const selectItem = new Select(select, {
    searchEnabled: false, // отключить поиск
    itemSelectText: '', // текст, который отображается для каждого выбранного элемента в множественном списке
    shouldSort: false, // отключить сортировку по алфавиту
    // position: 'select-one', // позиция только снизу
  });
  selectItem.init();
});

const works = document.querySelectorAll('.works-item');
const worksButton = document.getElementById('works-link');
const worksLoadingMore = new LoadingMoreItems(8, 8, works, worksButton);
worksLoadingMore.init();

serviceNavigationInit();
lb();
header();
headerNavigation();
headerSearch();
appear();
saturate();
countdown();
hairExtentionSign();
modal();
accordionMenu();
accordionItem();
footerDate();
ratingInit();
mrcSH();
new MultiStepCalculator().init();
new MultiStepCalculatorPrices();
replaceRubleSymbols(document.body);
new MultiStepCalculatorForm();
video360Init();
worksSliderInit();

const homeHeader = document.querySelector('.home-header .slider__slides');
if (homeHeader) {
  const interiorsSliderInstance = new Slider(homeHeader, {});
  interiorsSliderInstance.init();
}

const feedback = document.querySelector('.feedback-item .slider__slides');
if (feedback) {
  const feebackItem = document.querySelector('.feedback-item');
  const feedbackSlider = new Slider(feedback, {
    on: {
      reachEnd() {
        feebackItem.classList.add('feedback-item--transition-end');
      },
      slidePrevTransitionStart() {
        feebackItem.classList.remove('feedback-item--transition-end');
      },
    },
    watchOverflow: true,
    breakpoints: {
      480: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      800: {
        slidesPerView: 2.5,
        spaceBetween: 20,
      },
      1000: {
        slidesPerView: 2.5,
        spaceBetween: 30,
      },
    },
  });
  feedbackSlider.init();
  feebackItem.classList.remove('feedback-item--transition-end');
}

const articlePhotoSlider = document.querySelectorAll('.photo-item .slider__slides');
if (articlePhotoSlider) {
  articlePhotoSlider.forEach((slider) => {
    const articlePhotoSliderInstance = new Slider(slider, {});
    articlePhotoSliderInstance.init();
  });
}

const parallaxElements = [
  [document.querySelectorAll('.page-section--home-advantages'), '--before-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-services'), '--after-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-services'), '--before-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-brands'), '--before-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-staff'), '--before-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-works'), '--before-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-address'), '--before-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-yandex-reviews'), '--after-translate', -100, 200],
  [document.querySelectorAll('.page-section--home-why-we'), '--before-translate', -100, 200],
  [document.querySelectorAll('.home-appointment'), '--before-translate', -100, 100],
];

parallaxElements.forEach((element) => parallax(...element));
