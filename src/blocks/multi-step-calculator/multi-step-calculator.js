/* eslint-disable no-param-reassign */
import media from '../../scripts/modules/match-media';

export default class MultiStepCalculator {
  #element;
  #elementContainer;
  #slides;
  #prevButton;
  #nextButton;
  #yclientsButton;
  #paginationContainer;
  #paginationItems;
  #select;
  #currentStep = 0;
  #serviceSelect;
  #colorantSlide;
  #colorantTableRow;

  constructor() {
    this.#element = document.querySelector('.multi-step-calculator');
    this.#elementContainer = this.#element?.querySelector('.multi-step-calculator__inner');
    this.#slides = this.#element?.querySelectorAll('.multi-step-calculator__slide');
    this.#prevButton = this.#element?.querySelector('.multi-step-calculator__button--prev');
    this.#nextButton = this.#element?.querySelector('.multi-step-calculator__button--next');
    this.#yclientsButton = this.#element?.querySelector('.multi-step-calculator__button--yclients');
    this.#paginationContainer = this.#element?.querySelector('.multi-step-calculator__pagination');
    this.#select = this.#element?.querySelector('.multi-step-calculator__select');
    this.#serviceSelect = this.#element?.querySelector('.multi-step-calculator__slide--service select');
    this.#colorantSlide = this.#element?.querySelector('.multi-step-calculator__slide--colorant');
    this.#colorantTableRow = this.#element?.querySelector('.prices-table__row[data-colorant]').closest('.prices-table');
  }

  #changeSelectImages() {
    this.#select.addEventListener('change', (evt) => {
      evt.preventDefault();
      const imagePaths = {
        'Окраска волос в один тон': '/images/uslugi/one-tone.webp',
        'Сложное окрашивание': '/images/uslugi/complex.webp',
        'Снятие краски': '/images/uslugi/removal.webp',
        'Мелирование классическое': '/images/uslugi/classic.webp',
        'Мелирование классическое с тонированием': '/images/uslugi/toning.webp',
        'Яркое креативное окрашивание': '/images/uslugi/bright.webp',
        'Креативное окрашивание': '/images/uslugi/creative.webp',
      };
      const imageElement = this.#select.nextElementSibling;
      imageElement.src = imagePaths[evt.target.selectedOptions[0].value];
    });
  }

  #initPagination() {
    this.#slides.forEach(() => {
      const paginationItem = document.createElement('div');
      paginationItem.classList.add('multi-step-calculator__pagination-item');
      this.#paginationContainer.append(paginationItem);
    });
  }

  #showStep = (evt) => {
    if (evt.target.dataset.nav === 'prev' && this.#currentStep > 0) {
      this.#currentStep -= 1;
    } else if (evt.target.dataset.nav === 'next' && this.#currentStep < this.#slides.length - 1) {
      this.#currentStep += 1;
    }
    if (evt.target.dataset.nav === 'prev' || evt.target.dataset.nav === 'next') evt.preventDefault();
    this.#updateUI();
    this.#updateContainerHeight();
  };

  #updateContainerHeight = () => {
    media.addChangeListener('max', 'm', () => this.#updateContainerHeight());
    if (media.max('m')) {
      let maxHeight = 0;
      this.#slides.forEach((slide, index) => {
        if (index === this.#currentStep) {
          const slideHeight = slide.offsetHeight;
          if (slideHeight > maxHeight) maxHeight = slideHeight;
        }
      });
      this.#elementContainer.style.maxHeight = `${maxHeight}px`;
      window.addEventListener('resize', this.#updateContainerHeight);
    } else {
      this.#elementContainer.style.maxHeight = 'max-content';
      window.removeEventListener('resize', this.#updateContainerHeight);
    }
  };

  #updateUI() {
    this.#paginationItems = this.#paginationContainer?.querySelectorAll('.multi-step-calculator__pagination-item');
    this.#slides.forEach((slide, index) => {
      slide.style.transform = `translateX(-${this.#currentStep * 100}%)`;
      slide.classList.toggle('multi-step-calculator__slide--active', index === this.#currentStep);
    });
    this.#prevButton.classList.toggle('multi-step-calculator__button--hidden', this.#currentStep === 0);
    if (this.#currentStep === this.#slides.length - 1) {
      this.#yclientsButton.classList.remove('multi-step-calculator__button--hidden');
      this.#nextButton.classList.add('multi-step-calculator__button--hidden');
    } else {
      this.#yclientsButton.classList.add('multi-step-calculator__button--hidden');
      this.#nextButton.classList.remove('multi-step-calculator__button--hidden');
    }
    this.#paginationItems.forEach((pagination, index) => pagination.classList.toggle('multi-step-calculator__pagination-item--active', index === this.#currentStep));
  }

  #showHideColorant = () => {
    this.#serviceSelect.addEventListener('change', () => {
      const selectedService = this.#serviceSelect.selectedOptions[0].textContent.trim();
      const lastSlide = this.#elementContainer.querySelector('.multi-step-calculator__slide:last-of-type');
      if (selectedService === 'Окраска волос в один тон' && !this.#elementContainer.contains(this.#colorantSlide)) {
        this.#elementContainer.insertBefore(this.#colorantSlide, lastSlide);
        // this.#colorantTableRow.style.display = 'block';
      } else if (this.#elementContainer.contains(this.#colorantSlide)) {
        this.#elementContainer.removeChild(this.#colorantSlide);
        // this.#colorantTableRow.style.display = 'none';
      }
      this.#slides = this.#element?.querySelectorAll('.multi-step-calculator__slide');
      this.#updatePagination();
      this.#updateUI();
      this.#updateContainerHeight();
    });
  };

  #updatePagination = () => {
    this.#paginationContainer.innerHTML = '';
    this.#slides.forEach(() => {
      const paginationItem = document.createElement('div');
      paginationItem.classList.add('multi-step-calculator__pagination-item');
      this.#paginationContainer.append(paginationItem);
    });
    this.#paginationItems = this.#paginationContainer?.querySelectorAll('.multi-step-calculator__pagination-item');
  };

  init() {
    if (!this.#element) return;
    this.#changeSelectImages();
    this.#updateContainerHeight();
    this.#initPagination();
    this.#updateUI();
    this.#element.addEventListener('click', this.#showStep);
    this.#showHideColorant();
  }
}
