const countdowns = document.querySelectorAll('.countdown');

const getFirstDayOfNextMonth = (year, month) => new Date(year, month + 1, 1, 0, 0, 0);

const addCountdown = (deadline, countdownElement) => {
  let addTimer = null;
  const daysVal = countdownElement.querySelector('.countdown__days .countdown__val');
  const hoursVal = countdownElement.querySelector('.countdown__hours .countdown__val');
  const minutesVal = countdownElement.querySelector('.countdown__minutes .countdown__val');
  const secondsVal = countdownElement.querySelector('.countdown__seconds .countdown__val');

  const timeCount = () => {
    const now = new Date();
    let diff = deadline - now;

    if (diff <= 0) {
      // Останавливаем текущий таймер
      clearInterval(addTimer);

      // Если дедлайн текущего месяца прошел, обновляем его на первый день следующего месяца
      const nextMonth = getFirstDayOfNextMonth(deadline.getFullYear(), deadline.getMonth());
      deadline.setTime(nextMonth.getTime());

      // Запускаем таймер заново с обновленным дедлайном
      addTimer = setInterval(timeCount, 1000);
      diff = deadline - now; // Пересчитываем разницу
    }

    // Вычисляем дни, часы, минуты, секунды
    const days = diff > 0 ? (`0${Math.floor(diff / 1000 / 60 / 60 / 24)}`).slice(-2) : '00';
    const hours = diff > 0 ? (`0${Math.floor(diff / 1000 / 60 / 60) % 24}`).slice(-2) : '00';
    const minutes = diff > 0 ? (`0${Math.floor(diff / 1000 / 60) % 60}`).slice(-2) : '00';
    const seconds = diff > 0 ? (`0${Math.floor(diff / 1000) % 60}`).slice(-2) : '00';

    // Обновляем значения на странице
    daysVal.textContent = days;
    hoursVal.textContent = hours;
    minutesVal.textContent = minutes;
    secondsVal.textContent = seconds;
  };

  timeCount();
  addTimer = setInterval(timeCount, 1000);
};

const countdown = () => {
  const curDate = new Date();
  const firstDayOfNextMonth = getFirstDayOfNextMonth(curDate.getFullYear(), curDate.getMonth());

  countdowns.forEach((countdownElement) => {
    // eslint-disable-next-line max-len
    const timestampDate = countdownElement.dataset.date ? new Date(+countdownElement.dataset.date) : null;
    const deadline = timestampDate || firstDayOfNextMonth;
    addCountdown(deadline, countdownElement);
  });
};

export default countdown;
