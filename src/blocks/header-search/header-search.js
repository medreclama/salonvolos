const headerSearch = () => {
  const search = document.querySelector('.header-search');
  search.addEventListener('click', (e) => {
    if (e.target.classList.contains('header-search__switcher')) search.classList.toggle('header-search--active');
  });
};

export default headerSearch;
