/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
export default class MultiStepCalculatorPrices {
  #element;
  #tableRows;
  #serviceSelect;
  #additionalServiceInputs;
  #densityInputs;
  #colorantInputs;
  #lengthSelect;
  #newPrices;
  #oldPrices;
  #colorantTableRow;
  priceData = {};

  constructor() {
    this.#element = document.querySelector('.multi-step-calculator');
    this.#serviceSelect = this.#element?.querySelector('.multi-step-calculator__slide--service select');
    this.#lengthSelect = this.#element?.querySelector('.multi-step-calculator__slide--length select');
    this.#densityInputs = this.#element?.querySelectorAll('.multi-step-calculator__slide--density input[type="radio"]');
    this.#additionalServiceInputs = this.#element?.querySelectorAll('.multi-step-calculator__slide--additional-service input[type="radio"]');
    this.#colorantInputs = this.#element?.querySelectorAll('.multi-step-calculator__slide--colorant input[type="radio"]');
    this.#tableRows = document.querySelectorAll('.prices-table__row');
    this.#newPrices = this.#element?.querySelectorAll('.prices-table__new-price');
    this.#oldPrices = this.#element?.querySelectorAll('.prices-table__old-price');
    this.#colorantTableRow = this.#element?.querySelector('.prices-table__row[data-colorant] .prices-table__cell');
    this.previousColorantSelection = null;
    this.loadPriceData();
  }

  async loadPriceData() {
    try {
      const response = await fetch('https://www.salonvolos.spb.ru/getPriceData.php');
      const data = await response.json();
      this.priceData = data;
      this.init();
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error('Ошибка при загрузке данных:', error);
    }
  }

  #onChange(event, type) {
    const { target } = event;
    let selectedOption;
    if (target.tagName === 'SELECT') {
      const option = target.selectedOptions[0];
      selectedOption = option;
    } else if (target.type === 'radio' && target.checked) {
      selectedOption = target;
    }
    if (selectedOption) {
      if (type === 'service') {
        const selectedService = selectedOption.value;

        this.#colorantInputs.forEach((input) => {
          if (input.checked) this.previousColorantSelection = input.value;
        });

        this.#colorantInputs.forEach((input) => {
          input.checked = false;
        });

        if (selectedService === 'Окраска волос в один тон' && this.previousColorantSelection) {
          this.#colorantInputs.forEach((input) => {
            if (input.value === this.previousColorantSelection) {
              input.checked = true;
            }
          });
        }
      }
      this.#updateTableRow(selectedOption, type);
    }
    this.#calculateTotalPriceFromInputs();
    this.#calculateDiscount();
  }

  #updateTableRow(selectedOption, type) {
    const name = selectedOption.text ? selectedOption.text.trim() : selectedOption.value.trim();
    const { value } = selectedOption;
    const dataAttributesMap = {
      service: 'data-service',
      additionalService: 'data-additional-service',
      colorant: 'data-colorant',
      density: 'data-density',
    };
    const dataAttribute = dataAttributesMap[type];

    let price = 0;
    const lengthValue = this.#lengthSelect ? this.#lengthSelect.selectedOptions[0].value : null;

    if (type === 'colorant') {
      const colorantData = this.priceData['Окраска волос в один тон']?.[value];
      if (colorantData && typeof colorantData === 'object') {
        price = colorantData[lengthValue] || 0;
      }
    } else if (type === 'density') {
      price = this.priceData['Густота']?.[value] || 0;
    } else if (type === 'service') {
      const serviceData = this.priceData[value];
      if (serviceData && typeof serviceData === 'object') {
        const basePrice = serviceData.basePrice || 0;
        const lengthPrice = serviceData[lengthValue] || 0;

        let colorantPrice = 0;
        this.#colorantInputs.forEach((input) => {
          if (input.checked) {
            const selectedColorant = input.value;
            const colorantData = serviceData[selectedColorant];
            if (colorantData && typeof colorantData === 'object') {
              colorantPrice = colorantData[lengthValue] || 0;
            }
          }
        });

        price = basePrice + lengthPrice + colorantPrice;
      }
    } else {
      price = this.priceData[value];
      if (typeof price === 'object' && lengthValue) {
        price = (price.basePrice || 0) + (price[lengthValue] || 0);
      } else {
        price = price || 0;
      }
    }

    // Обновляем таблицу
    this.#tableRows.forEach((row) => {
      if (row.hasAttribute(dataAttribute)) {
        const rowHeader = row.querySelector('.prices-table__cell--row-header');
        const oldPrice = row.querySelector('.prices-table__old-price');
        const newPrice = row.querySelector('.prices-table__new-price');
        rowHeader.innerHTML = name;
        if (oldPrice) oldPrice.innerHTML = `${price.toLocaleString('ru-RU')} ₽`;
        else newPrice.innerHTML = `${price.toLocaleString('ru-RU')} ₽`;
      }
    });
  }

  #calculateTotalPriceFromInputs() {
    let totalPrice = 0;

    // Учёт основной услуги с учётом длины
    if (this.#serviceSelect) {
      const selectedService = this.#serviceSelect.selectedOptions[0].value;
      const selectedLength = this.#lengthSelect ? this.#lengthSelect.selectedOptions[0].value : null;
      const servicePriceData = this.priceData[selectedService];

      let servicePrice = servicePriceData?.basePrice || 0;

      // Учитываем длину, если она выбрана и присутствует в priceData
      if (selectedLength && servicePriceData?.[selectedLength]) {
        servicePrice += servicePriceData[selectedLength];
      }

      // добавляем стоимость красителя, если он выбран
      let colorantPrice = 0;
      this.#colorantInputs.forEach((input) => {
        if (input.checked) {
          const selectedColorant = input.value;
          const colorantData = this.priceData[selectedService]?.[selectedColorant];
          if (colorantData && typeof colorantData === 'object') {
            colorantPrice = colorantData[selectedLength] || 0;
          }
        }
      });

      servicePrice += colorantPrice;
      totalPrice += servicePrice;
    }

    // Обрабатываем все выбранные дополнительные услуги
    this.#additionalServiceInputs.forEach((input) => {
      if (input.checked) {
        const selectedAdditionalService = input.value;
        const selectedLength = this.#lengthSelect ? this.#lengthSelect.selectedOptions[0].value : null;
        const additionalServicePriceData = this.priceData[selectedAdditionalService];

        let additionalServicePrice = additionalServicePriceData?.basePrice || 0;

        // Учитываем длину волос для всех доп. услуг, если это возможно
        if (selectedLength && additionalServicePriceData?.[selectedLength]) {
          additionalServicePrice += additionalServicePriceData[selectedLength];
        }

        totalPrice += additionalServicePrice;
      }
    });

    // Учёт густоты
    this.#densityInputs.forEach((input) => {
      if (input.checked) {
        const selectedDensity = input.value;
        const densityPrice = this.priceData['Густота']?.[selectedDensity] || 0;
        totalPrice += densityPrice;
      }
    });

    // Обновляем таблицу с итоговой ценой
    this.#tableRows.forEach((row) => {
      if (row.hasAttribute('data-total-price')) {
        const oldPrice = row.querySelector('.prices-table__old-price');
        if (oldPrice) {
          oldPrice.innerHTML = `${totalPrice.toLocaleString('ru-RU')} ₽`;
        }
      }
    });
  }

  #calculateDiscount() {
    this.#oldPrices.forEach((oldPrice, index) => {
      const oldPriceValue = parseFloat(oldPrice.innerHTML.replace(/[^0-9.,]/g, '').replace(',', '.'));
      const discount = oldPriceValue * 0.1;
      const newPriceValue = oldPriceValue - discount;
      this.#newPrices[index].innerHTML = `${Math.round(newPriceValue).toLocaleString('ru-RU')} ₽`;
    });
  }

  init() {
    if (!this.#element) return;
    this.#colorantTableRow.closest('.prices-table').style.display = 'none';
    const initialSelectedOption = this.#serviceSelect?.selectedOptions[0];
    this.#updateTableRow(initialSelectedOption, 'service');
    this.#serviceSelect.addEventListener('change', (event) => {
      this.#calculateTotalPriceFromInputs();
      this.#onChange(event, 'service');
    });

    this.#lengthSelect.addEventListener('change', (event) => {
      this.#calculateTotalPriceFromInputs();
      this.#onChange(event, 'length');
      if (this.#serviceSelect) {
        const selectedService = this.#serviceSelect.selectedOptions[0];
        this.#updateTableRow(selectedService, 'service');
      }
      this.#additionalServiceInputs.forEach((input) => {
        if (input.checked) this.#updateTableRow(input, 'additionalService');
      });
      this.#colorantInputs.forEach((input) => {
        if (input.checked) this.#updateTableRow(input, 'colorant');
      });
    });

    this.#additionalServiceInputs.forEach((input) => {
      if (input.checked) this.#updateTableRow(input, 'additionalService');
      input.addEventListener('change', (event) => {
        this.#onChange(event, 'additionalService');
        const additionalServiceTableRow = document.querySelector('.prices-table__row[data-additional-service]').closest('.prices-table');
        if (input.value === 'Не требуются') additionalServiceTableRow.style.display = 'none';
        else additionalServiceTableRow.style.display = 'block';
      });
    });

    this.#colorantInputs.forEach((input) => {
      input.addEventListener('change', (event) => {
        if (this.#serviceSelect) {
          const selectedService = this.#serviceSelect.selectedOptions[0];
          this.#updateTableRow(selectedService, 'service');
        }
        this.#onChange(event, 'colorant');
        if (input.value === 'Не имеет значения') this.#colorantTableRow.innerHTML = 'Краситель не имеет значения';
      });
    });

    this.#densityInputs.forEach((input) => {
      if (input.checked) this.#updateTableRow(input, 'density');
      input.addEventListener('change', (event) => this.#onChange(event, 'density'));
    });

    this.#calculateTotalPriceFromInputs();
    this.#calculateDiscount();
  }
}
