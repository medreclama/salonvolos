const hairExtentionContainer = document.querySelector('.hair-extention-appointment');
const hairExtentionTitles = document.querySelectorAll('.hair-extention-sign__header span');
const hairExtentionOnlineButton = document.querySelector('.prices-block__record-online');
const hiddenInput = document.querySelector('.hair-extention-sign-form__input-type input');

const hairExtentionSign = () => {
  if (!hairExtentionContainer) return;
  hairExtentionContainer.addEventListener('click', (evt) => {
    const button = evt.target;
    if (button.classList.contains('hair-extention-appointment__button')) {
      hairExtentionTitles.forEach((title) => {
        const item = title;
        const extensionType = evt.target.dataset.name;
        item.innerHTML = `<strong>${extensionType}</strong>`;
        hiddenInput.setAttribute('value', extensionType);
      });
    }
  });
  if (hairExtentionOnlineButton) {
    hairExtentionOnlineButton.addEventListener('click', () => {
      document.querySelector('.hair-extention-sign__header span').innerHTML = '<strong>наращивание</strong>';
      hiddenInput.setAttribute('value', 'наращивание');
    });
  }
};

export default hairExtentionSign;
