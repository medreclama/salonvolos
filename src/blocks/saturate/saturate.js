import iivp from '../../scripts/modules/isInViewport';

const setTransform = (elem) => {
  if (iivp(elem)) {
    const scrollStart = window.innerHeight - (window.innerHeight / 6);
    const scrollEnd = window.innerHeight - (window.innerHeight / 4);
    const blockTop = elem.getBoundingClientRect().top;
    const colorLimit = getComputedStyle(elem).getPropertyValue('--saturate-limit');
    let color;
    if (blockTop >= scrollStart) {
      color = 0;
    } else if (blockTop < scrollStart && blockTop > scrollEnd) {
      color = colorLimit * (1 - (blockTop - scrollEnd) / (scrollStart - scrollEnd));
    } else {
      color = colorLimit;
    }
    elem.style.setProperty('--saturate', `${color}%`);
  }
};

const saturate = () => {
  const elems = document.querySelectorAll('.saturate');

  elems.forEach((elem) => setTransform(elem));

  window.addEventListener('scroll', () => requestAnimationFrame(() => {
    elems.forEach((elem) => setTransform(elem));
  }));
};

export default saturate;
