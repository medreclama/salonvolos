const addStarGradient = (container, rating) => {
  container.forEach((star, i) => {
    if (i < Math.floor(rating)) star.style.setProperty('--gradient-stop', '100%');
    else if (i === Math.floor(rating)) star.style.setProperty('--gradient-stop', `${(rating - Math.floor(rating)) * 100}%`);
  });
};

const ratingInit = () => {
  const ratingList = document.querySelectorAll('.rating');
  ratingList.forEach((rating) => {
    const stars = Array.from(rating.querySelectorAll('.rating__star'));
    addStarGradient(stars, rating.dataset.rating);
  });
};

export default ratingInit;
