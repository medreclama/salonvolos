const headerNavigation = () => {
  const parents = document.querySelectorAll('.header-navigation__item--parent');
  Array.from(parents).forEach((parent) => {
    const link = parent.firstElementChild;
    const opener = document.createElement('div');
    opener.classList.add('header-navigation__opener');
    opener.addEventListener('click', (e) => {
      e.target.classList.toggle('header-navigation__opener--active');
    });
    link.after(opener);
  });
};

export default headerNavigation;
