/* eslint-disable no-param-reassign */
const replaceRubleSymbols = (node) => {
  if (!node) return;
  const regex = /(?<=\s|^)(р\.|руб\.)/gi;
  if (node.nodeType === Node.TEXT_NODE) {
    const replacedText = node.nodeValue.replace(regex, '₽');
    if (replacedText !== node.nodeValue) {
      node.nodeValue = replacedText;
    }
  }

  if (node.nodeType === Node.ELEMENT_NODE) node.childNodes.forEach(replaceRubleSymbols);
};

export default replaceRubleSymbols;
