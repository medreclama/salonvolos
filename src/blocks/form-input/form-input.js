import Choices from 'choices.js';

class Select {
  #select;
  #element;
  #recievedSettings;

  #settings = {
    classNames: {
      containerOuter: 'choices form-input__select',
      containerInner: 'choices__inner form-input__select-inner',
      item: 'choices__item form-input__select-item',
    },
  };

  constructor(element, settings = {}) {
    this.#element = element;
    this.#recievedSettings = settings;
    Object.assign(this.#settings, this.#recievedSettings);
  }

  init() {
    this.#select = new Choices(this.#element, this.#settings);
  }
}

export default Select;
