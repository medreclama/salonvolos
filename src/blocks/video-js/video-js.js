const video360Init = () => {
  const video360 = document.getElementById('video360');
  if (!video360) return;
  // eslint-disable-next-line no-undef
  const player = videojs('video360', {
    controls: true,
    autoplay: false,
    playsinline: true,
    preload: 'auto',
    loop: true,
    aspectRatio: '16:9',
  });

  player.on('ready', () => {
    player.removeClass('vjs-user-inactive');
    player.on('mousemove', () => {
      player.removeClass('vjs-user-inactive');
    });

    player.on('touchstart', () => {
      player.removeClass('vjs-user-inactive');
    });

    player.on('touchmove', () => {
      player.removeClass('vjs-user-inactive');
    });

    player.on('play', () => {
      player.removeClass('vjs-user-inactive');
    });
  });

  player.vr({
    projection: '360',
    debug: false,
  });
};

export default video360Init;
