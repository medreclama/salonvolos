export default class MultiStepCalculatorForm {
  #element = document.querySelector('.multi-step-calculator');
  #comment = document.querySelector('.multi-step-calculator-form__comment textarea');
  #selects = this.#element?.querySelectorAll('.multi-step-calculator__select select');
  #radioButtons = this.#element?.querySelectorAll('.multi-step-calculator__slide input[type="radio"]');
  #oldPrice = this.#element?.querySelector('[data-total-price] .prices-table__old-price');
  #newPrice = this.#element?.querySelector('[data-total-price] .prices-table__new-price');

  constructor() {
    this.init();
  }

  #collectFormData() {
    const data = [];

    this.#selects.forEach((select) => {
      const label = select.closest('.multi-step-calculator__slide').querySelector('.multi-step-calculator__title').innerHTML.trim();
      const { value } = select;
      data.push(`${label}: ${value}`);
    });

    const checkedRadioButtons = [...this.#radioButtons].filter((radio) => radio.checked);
    checkedRadioButtons.forEach((radio) => {
      const label = radio.closest('.multi-step-calculator__slide').querySelector('.multi-step-calculator__title').innerHTML.trim();
      const { value } = radio;
      data.push(`${label}: ${value}`);
    });

    const oldPrice = this.#oldPrice.textContent.replace(/&nbsp;/g, ' ').trim();
    const newPrice = this.#newPrice.textContent.replace(/&nbsp;/g, ' ').trim();
    data.push(`Общая стоимость: ${oldPrice}`);
    data.push(`Стоимость со скидкой: ${newPrice}`);
    return data;
  }

  init() {
    if (!this.#element) return;
    const observer = new MutationObserver(() => {
      this.#comment.value = this.#collectFormData().join('\n');
    });

    observer.observe(this.#oldPrice, { childList: true, subtree: true });
    observer.observe(this.#newPrice, { childList: true, subtree: true });
    this.#element.addEventListener('change', () => {
      this.#comment.value = this.#collectFormData().join('\n');
    });
  }
}
