import LoadingMoreItems from '../../scripts/modules/LoadingMoreItems';

// Первоначальная загрузка отзывов
const feedbackItems = document.querySelectorAll('.feedback-item__slide');
const feedbackButton = document.querySelector('.loading-button');
const feedbacksLoadingMore = new LoadingMoreItems(6, 4, feedbackItems, feedbackButton);
feedbacksLoadingMore.init();

// Первоначальная загрузка работ
const worksItem = document.querySelectorAll('.works-item');
const worksButton = document.getElementById('works-button');
const worksLoadingMore = new LoadingMoreItems(8, 8, worksItem, worksButton);
worksLoadingMore.init();

const message = document.createElement('p');
message.classList.add('page-subsection');

const serviceNavigation = document.querySelector('.service-navigation');
const masterSelect = document.querySelector('.service-navigation__item--master select');
const siteSelect = document.querySelector('.service-navigation__item--site select');

let selectedService = 'all'; // По умолчанию выбраны все услуги
let selectedMaster = ''; // По умолчанию мастер не выбран
let selectedSite = ''; // По умолчанию сайт не выбран

// Сортировка отзывов на странице отзывов
const feedbackSorting = () => {
  message.innerHTML = 'Отзывы не найдены';
  feedbacksLoadingMore.destroy(); // Дестрой показа порции отзывов после выбора услуги/мастера
  // Отображаем только отзывы, соответствующие выбранным услуге и мастеру
  feedbackItems.forEach((feedback) => {
    const item = feedback;
    const { service } = item.dataset;
    const services = service.split(', ');
    const { master } = item.dataset;
    const { site } = item.dataset;

    if ((selectedService === 'all' || services.includes(selectedService)) && (selectedMaster === '' || selectedMaster === master) && (selectedSite === '' || selectedSite === site)) { // Проверяем услугу или мастера
      item.style.display = 'flex';
      message.remove();
    } else item.style.display = 'none';
  });
  // Пересчитываем количество видимых элементов и инициализируем экземпляр LoadingMoreItems
  const visibleItems = document.querySelectorAll('.feedback-item__slide[style="display: flex;"]');
  feedbacksLoadingMore.updateElementsList(visibleItems);
  feedbacksLoadingMore.init();
};

// Сортировка работ на странице работ
const worksSorting = () => {
  message.innerHTML = 'Работы не найдены';
  worksLoadingMore.destroy();
  worksItem.forEach((workItem) => {
    const item = workItem;
    const { service } = item.dataset;
    const services = service.split(', ');
    if ((selectedService === 'all' || services.includes(selectedService))) { // Проверяем услугу
      item.style.display = 'flex';
      message.remove();
    } else item.style.display = 'none';
  });
  // Пересчитываем количество видимых элементов и инициализируем экземпляр LoadingMoreItems
  const visibleItems = document.querySelectorAll('.works-item[style="display: flex;"]');
  worksLoadingMore.updateElementsList(visibleItems);
  worksLoadingMore.init();
};

// Сортировка
const updateSelection = (evt) => {
  evt.preventDefault();
  // Обновляем выбранную услугу, если клик произошел по кнопке с услугой
  if (evt.target.tagName === 'A') {
    selectedService = evt.target.dataset.service;
    const links = serviceNavigation.querySelectorAll('.button.service-navigation__item');
    links.forEach((link) => link.classList.add('button--secondary'));
    if (evt.target.classList.contains('button--secondary')) evt.target.classList.remove('button--secondary');
  }
  // Обновляем выбранного мастера при изменении значения в списке мастеров
  if (evt.target === masterSelect) selectedMaster = masterSelect.value;
  // Обновляем выбранный сайт при изменении значения в списке сайтов
  if (evt.target === siteSelect) selectedSite = siteSelect.value;

  serviceNavigation.after(message);
};

const updateFeedbacks = (evt) => {
  updateSelection(evt);
  feedbackSorting();
};

const updateWorks = (evt) => {
  updateSelection(evt);
  worksSorting();
};

const serviceNavigationInit = () => {
  if (serviceNavigation && masterSelect) {
    serviceNavigation.addEventListener('click', updateFeedbacks);
    masterSelect.addEventListener('change', updateFeedbacks);
    siteSelect.addEventListener('change', updateFeedbacks);
  }
  if (serviceNavigation && !masterSelect) {
    serviceNavigation.addEventListener('click', updateWorks);
  }
};

export default serviceNavigationInit;
