export const accordionMenu = () => {
  const serviceHeader = document.querySelector('.service-header');
  if (!serviceHeader) return;
  const menuSection = document.createElement('section');
  menuSection.classList.add('container', 'page-section', 'page-section--no-padding--top');

  const menu = document.createElement('div');
  menu.classList.add('accordion-item', 'accordion-item--menu', 'content');

  const menuHeader = document.createElement('h2');
  menuHeader.innerHTML = 'Содержание<svg class="accordion-item__arrow" width="16" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round"><animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate><animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate></path></svg>';
  menuHeader.classList.add('accordion-item__header');

  const menuBody = document.createElement('div');
  menuBody.classList.add('accordion-item__body');

  serviceHeader.after(menuSection);
  menuSection.append(menu);
  menu.append(menuHeader);
  menuHeader.after(menuBody);

  const elements = Array.from(document.querySelectorAll('h2:not(.hair-extention-sign__header):not(.accordion-item__header):not(.not-menu-content), [data-menu]'));

  elements.forEach((element, index) => {
    if (element.tagName.toLowerCase() === 'h2') {
      element.setAttribute('id', `title-${index}`);
      const link = document.createElement('a');
      link.classList.add('accordion-item__link');
      link.setAttribute('href', `#${element.id}`);
      link.innerHTML = element.innerHTML;
      menuBody.append(link);
    }

    if (element.hasAttribute('data-menu')) {
      const blockId = element.id || `menu-block-${index}`;
      element.setAttribute('id', blockId);
      const link = document.createElement('a');
      link.classList.add('accordion-item__link');
      link.setAttribute('href', `#${blockId}`);
      link.innerHTML = element.dataset.menu;
      menuBody.append(link);
    }
  });
};

export const accordionItem = () => {
  const items = document.querySelectorAll('.accordion-item');
  if (!items) return;
  items.forEach((item) => {
    item.addEventListener('click', (event) => {
      const arrow = item.querySelector('.accordion-item__arrow');
      const arrowAnimateOpen = arrow.querySelector('#open');
      const arrowAnimateClose = arrow.querySelector('#close');
      if (event.target.closest('.accordion-item__header')) item.classList.toggle('accordion-item--active');
      if (item.classList.contains('accordion-item--active')) arrowAnimateOpen.beginElement();
      else arrowAnimateClose.beginElement();
    });
  });
};
