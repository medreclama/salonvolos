export default class LoadingMoreItems {
  #elementsQuantity;
  #elementsList;
  #loadingButton;
  #currentIndex;
  #elementsToShow;

  constructor(elementsQuantity, elementsToShow, elementsList, loadingButton) {
    this.#elementsQuantity = elementsQuantity;
    this.#elementsList = Array.from(elementsList);
    this.#loadingButton = loadingButton;
    this.#currentIndex = elementsQuantity; // Начинаем с элемента #elementsQuantity
    this.#elementsToShow = elementsToShow;
  }

  updateElementsList(newElementsList) {
    this.#elementsList = Array.from(newElementsList);
  }

  #handleShowMoreButtonClick = (evt) => {
    evt.preventDefault();
    // eslint-disable-next-line max-len
    const itemsToShow = this.#elementsList.slice(this.#currentIndex, this.#currentIndex + this.#elementsToShow);
    itemsToShow.forEach((item) => {
      const element = item;
      element.style.display = 'flex';
    });
    this.#currentIndex += this.#elementsToShow;
    if (this.#currentIndex >= this.#elementsList.length) this.#loadingButton.style.display = 'none';
  };

  init() {
    if (!this.#loadingButton) return;

    this.#elementsList.forEach((item, index) => {
      if (index >= this.#elementsQuantity) { // Показываем элементы, начиная с #elementsQuantity
        const element = item;
        element.style.display = 'none';
      }
    });

    if (this.#elementsList.length > this.#elementsQuantity) {
      this.#loadingButton.addEventListener('click', this.#handleShowMoreButtonClick);
      // Показываем кнопку, если она скрыта
      if (this.#loadingButton.style.display === 'none') this.#loadingButton.style.display = 'block';
    } else {
      this.#loadingButton.style.display = 'none';
    }
  }

  destroy() {
    this.#elementsList.forEach((item) => {
      const element = item;
      element.style.display = 'flex';
    });
    // eslint-disable-next-line max-len
    this.#currentIndex = this.#elementsQuantity; // Сбрасываем индекс текущего элемента обратно на начальное значение
  }
}
