import iivp from '../../scripts/modules/isInViewport';

const setTransform = (elem) => {
  if (iivp(elem)) {
    const scrollStart = window.innerHeight - (window.innerHeight / 20);
    const scrollEnd = window.innerHeight - (window.innerHeight / 5);
    const blockTop = elem.getBoundingClientRect().top;
    let pos = ((document.documentElement.clientWidth / 2)
      - (elem.getBoundingClientRect().left + (elem.offsetWidth / 2))) / 50;
    pos = pos < 2 && pos > -2 ? 0 : pos;
    const translateBegin = 32 * -Math.sign(pos);

    let opacity;
    let scale;
    let translate;

    if (blockTop >= scrollStart) {
      opacity = 0;
      scale = 1.0675;
      translate = translateBegin;
    } else if (blockTop < scrollStart && blockTop > scrollEnd) {
      opacity = 1 - (blockTop - scrollEnd) / (scrollStart - scrollEnd);
      scale = 1 + (0.0675 * ((blockTop - scrollEnd) / (scrollStart - scrollEnd)));
      translate = translateBegin * ((blockTop - scrollEnd) / (scrollStart - scrollEnd));
    } else {
      opacity = 1;
      scale = 1;
      translate = 0;
    }
    elem.style.setProperty('--opacity', opacity);
    elem.style.setProperty('--scale', scale);
    elem.style.setProperty('--translateX', `${translate}px`);
  }
};

const appear = () => {
  const arr = document.querySelectorAll('.appear');

  arr.forEach((elem) => setTransform(elem));

  window.addEventListener('scroll', () => requestAnimationFrame(() => {
    arr.forEach((elem) => setTransform(elem));
  }));
};

export default appear;
