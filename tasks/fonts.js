import gulp from 'gulp';
import ttf2woff2 from 'gulp-ttf2woff2';
import changed from 'gulp-changed';

import { templateDir } from '../gulpfile.babel';

gulp.task('fonts', (done) => {
  gulp.src(['src/fonts/*.ttf'])
    .pipe(changed(`${templateDir}/fonts/`))
    .pipe(ttf2woff2())
    .pipe(gulp.dest(`${templateDir}/fonts/`));
  done();
});
