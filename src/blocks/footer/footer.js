const footerDate = () => {
  const footerElement = document.querySelector('.footer__name');
  if (!footerElement) return;
  const currentYear = new Date().getFullYear();
  let footerName = footerElement.innerHTML;
  footerName = footerName.replace(/\d{4}$/, currentYear);
  footerElement.innerHTML = footerName;
};

export default footerDate;
